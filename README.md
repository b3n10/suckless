# dwm patches
- [columns](https://dwm.suckless.org/patches/columns/)
- [pertag](https://dwm.suckless.org/patches/pertag/)
- [focusonclick](https://dwm.suckless.org/patches/focusonclick/)
- [cfacts](https://dwm.suckless.org/patches/cfacts/)
- [alwayscenter](https://dwm.suckless.org/patches/alwayscenter/)

# st patches
- [anysize](https://st.suckless.org/patches/anysize/) - if latest patch is not working, use [0.8.1 patch](https://st.suckless.org/patches/anysize/st-anysize-0.8.1.diff)
- ~~[st-scrollback-0.8.diff](https://st.suckless.org/patches/scrollback/)~~
- ~~[keyboard_select](https://st.suckless.org/patches/keyboard_select/)~~
- [copyurl](https://st.suckless.org/patches/copyurl/)

# Others
- Use [dmenu_run_history](https://tools.suckless.org/dmenu/scripts/dmenu_run_with_command_history/) instead of dmenu_run
    - make sure it's in $PATH or link it to ~/.local/bin
